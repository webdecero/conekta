<form action="{{route('page.savePaymentMethod')}}" method="POST" id="card-form">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <input type="hidden" name="_conekta_key" id="_conekta_key" value="{{ config('manager.conekta.api_key_front')}}">
    <input type="hidden" name="userID" value="{{$idUser}}">
    <input type="hidden" name="edit" value="1">
    <div class="row">

       
        <div class="field form-group col-lg-12 show-bottom">
            <label>Nombre del tarjetahabiente</label>
            <input class="nombre-tarjeta" type="text" size="20" data-conekta="card[name]" autocomplete='a-nombre' required>
            <div class="help-block with-errors"></div>
        </div>
        <div class="field form-group col-lg-12 show-bottom">
            <label class="d-inline mr-2" >Número de tarjeta de crédito</label> <i class="fab fa-cc-mastercard fa-lg"></i> <i class="fab fa-cc-visa fa-lg"></i>
            <input class="numero-tarjeta" name="cardnumber" type="text" size="20" data-conekta="card[number]" autocomplete='a-numeroTC' required>
            <div class="help-block with-errors"></div>
        </div>
        <div class="field form-group col-lg-12 show-bottom">
            <label>CVC</label>
            <input class="cvv-tarjeta" name="cvcnumber" type="text" size="4" data-conekta="card[cvc]" autocomplete='a-cvc' required>
            <div class="help-block with-errors"></div>
        </div>
        <div class="field form-group col-lg-12 show-bottom">
            <label>Fecha de expiración (MM/AAAA)</label>
            <input name="monthcard" class="col-5 col-sm-4 mes-tarjeta" type="text" size="2" data-conekta="card[exp_month]" autocomplete='a-mes' maxlength="2" required>
            <span class="col-1 col-sm-1">/</span>
            <input name="yearcard" class="col-5 col-sm-4 ano-tarjeta" type="text" size="4" data-conekta="card[exp_year]" autocomplete='a-anio' maxlength="4" required>
            <div class="help-block with-errors"></div>
            <div class="help-block with-errors"></div>
        </div>

        <div class="field form-group col-lg-12 show-bottom">
            <span class="card-errors"></span>
        </div>

        <button  class="button purple btn-common-cont nav-link mt-3 mx-2">Continuar</button>
        <a href = "{{route('page.index')}}" class="btn button purple btn-common-cont nav-link mt-3 mx-2 ">Regresar</a>
</form>