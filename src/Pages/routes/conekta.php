<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//conekta
//Route::get('conekta/detail/{id}', ['as' => 'conekta.detail', 'uses' => 'PageController@payDetail']);
//Route::get('conekta/reply/{id}', ['as' => 'conekta.reply', 'uses' => 'PageController@payReply']);

//Route::post('conekta', ['as' => 'conekta.payment', 'uses' => 'PayController@postPayment']);
//Route::post('conekta/registry', ['as' => 'conekta.registry', 'uses' => 'PayController@postUserRegitry']);

//Route::get('conekta/response/payment/{id}/{success}', ['as' => 'conekta.response.payment', 'uses' => 'PayController@getResponsePayment']);
//Route::get('conekta/response/plan/{id}/{success}', ['as' => 'conekta.response.plan', 'uses' => 'PayController@getResponsePlan']);




Route::group(['middleware' => ['api'], 'prefix' => 'conekta'], function () {
    
    if (!Route::has('conekta.api.webhook')){
            Route::any('v1/webhook', ['as' => 'conekta.api.webhook', 'uses' => 'ConektaWebhookController@index']);
            
            
    }

    
    
});