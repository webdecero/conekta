<?php

namespace Webdecero\Conekta\Pages\Controllers;

use Webdecero\Conekta\Manager\Models\ConektaCharge;
use Webdecero\Conekta\Manager\Models\ConektaGeneralLog;
use Webdecero\Conekta\Manager\Models\ConektaOrder;
use Webdecero\Conekta\Manager\Models\ConektaPlan;
use Webdecero\Conekta\Manager\Models\ConektaSubscription;
use Webdecero\Conekta\Manager\Models\ConektaSubscriptionPayment;
use Webdecero\Base\Pages\Controllers\PagesController as WebdeceroPageController;
use Webdecero\Conekta\Pages\Controllers\WebhookInterface;
use Illuminate\Http\Request;

class ConektaWebhookController extends WebdeceroPageController implements WebhookInterface {

    protected $inputRequest = [];
    protected $configConektaUser;
    protected $currentLog;

    function __construct() {
        parent::__construct();

        $this->configConektaUser = config('manager.conekta.userRelationships');
    }

    /*     * 0
     * Function to listen a webhook and process any response
     * @param Request $request
     */

    protected function index(Request $request) {
        $input = $request->all();

//        $txt = '
//       {
//        "data" : {
//            "object" : {
//                "id" : "5ba30b1cb795b01358bd11a6", 
//                "livemode" : false, 
//                "created_at" : NumberInt(1537411868), 
//                "currency" : "MXN", 
//                "description" : "testplan", 
//                "reference_id" : null, 
//                "failure_code" : null, 
//                "failure_message" : null, 
//                "monthly_installments" : null, 
//                "refunds" : [
//
//                ], 
//                "payment_method" : {
//                    "name" : "uriel", 
//                    "exp_month" : "05", 
//                    "exp_year" : "20", 
//                    "auth_code" : "781141", 
//                    "object" : "card_payment", 
//                    "last4" : "4242", 
//                    "brand" : "visa"
//                }, 
//                "details" : {
//                    "name" : "Uriel Acosta", 
//                    "phone" : "+525544518595", 
//                    "email" : "uriel@webdecero.com", 
//                    "line_items" : [
//
//                    ]
//                }, 
//                "object" : "charge", 
//                "status" : "paid", 
//                "amount" : NumberInt(300), 
//                "paid_at" : NumberInt(1537411869), 
//                "fee" : NumberInt(300), 
//                "customer_id" : "cus_2jJFj576JtcHZLQSo"
//            }, 
//            "previous_attributes" : {
//                "payment_method" : [
//
//                ]
//            }
//        }, 
//        "livemode" : false, 
//        "webhook_status" : "failing", 
//        "webhook_logs" : [
//            {
//                "id" : "webhl_2jJFk9rjZqL2YMhaf", 
//                "url" : "https://syb.basha.mx/api/v1/v1/webhook", 
//                "failed_attempts" : NumberInt(0), 
//                "last_http_response_status" : NumberInt(-1), 
//                "response_data" : [
//
//                ], 
//                "object" : "webhook_log", 
//                "last_attempted_at" : NumberInt(1537450295)
//            }
//        ], 
//        "id" : "5ba30b1db795b01358bd11af", 
//        "object" : "event", 
//        "type" : "charge.paid", 
//        "created_at" : NumberInt(1537411869)
//    }
//        ';
//
//
//        $input = json_decode($txt, true);


        $this->currentLog = $this->createGeneralLog($input);
        $eventType = $input['type'];
        try{
            switch ($eventType) {
                //Suscricion
                case 'subscription.created':    //ok
                    $this->createSubscription($input);
                    break;
                case 'subscription.paid':       //ok
                    $this->suscriptionPayment($input, true);
                    break;
                case 'subscription.canceled':   //ok
                    $this->subscriptionCanceledModule($input);
                    break;
                case 'subscription.payment_failed': //ok
                    $this->suscriptionPayment($input, false);
                    break;
                // Cargos
                case 'charge.created':  //ok
                    $this->chargeCreated($input);
                    break;
                case 'charge.paid':     //ok
                    $this->chargePaidModule($input);
                    break;
                case 'charge.refunded':
                    $this->chargeChargebackCreatedModule($input);
                    break;
                case 'charge.chargeback.created':
                    $this->chargeChargebackCreatedModule($input);
                    break;
                // Ordenes
                case 'order.created':   //ok
                    $this->orderCreatedModule($input);
                    break;
                case 'order.paid':  //ok
                    $this->orderPaidModule($input);
                    break;
                case 'order.pending_payment':
                    $this->orderPendingPaymentModule($input);
                    break;
                case 'order.expired':
                    $this->orderExpiredModule($input);
                    break;
                //Plan
                case 'plan.created':    //ok                                                 
                    $this->createPlanModule($input);
                    break;
                case 'plan.updated':    //ok
                    $this->updatePlanModule($input);
                    break;
                case 'plan.deleted':    //ok
                    $this->deletePlanModule($input);
                    break;
            }
        } catch (Exception $e){
            $msg = $ex->getMesage();
            $exceptionLog = new ConektaGeneralLog();
            $exceptionLog->data = $input;
            $exceptionLog->eventType = 'EXCEPTION LOG';
            $exceptionLog->transactionID = $input['id'];
            $exceptionLog->trace = $msg;
            $exceptionLog->save();
        }
    }

    public function createSubscription($input) {
        $subscriptionData = $input['data']['object'];
        $subscription = ConektaSubscription::where('subscriptionId', $subscriptionData['id'])->first();
        $subscription->data = $input;
        $subscription->eventType = $input['type'];
        $subscription->transactionID = $input['id'];
        $subscription->statusDescription = $subscriptionData['status'];
        $subscription->createdAt = $subscriptionData['created_at'];
        $subscription->trialEnd = $subscriptionData['created_at'];
        $planID = $subscriptionData['plan_id'];
        $subscription->plan_id = $planID;
        $customerID = $subscriptionData['customer_id'];
        $subscription->conekta_id = $customerID;
        $subscription->descriptionConekta = "SUBCRIPCION CREADA CONFIRMADA POR CONEKTA";
        //Se hacen las relaciones
        $subscription->save();
        //Se busca el plan
//        $plan = ConektaPlan::where('planId', $planID)->first();
        //Se busca el usuario
//        $userClass = $this->configConektaUser;
//        $user = $userClass::where('conekta_id', $customerID)->first();
//        $plan->conektaSubscriptions()->save($subscription);
//        $user->conektaSubscription()->save($subscription);
        $subscription->conektaLogs()->save($this->currentLog);
        $this->callBackCreateSubscription($subscription);
    }

    public function chargeCreated($input) {
        
//        dd("here");
        
        $chargeData = $input['data']['object'];
        $charge = ConektaCharge::where('chargeId', $chargeData['id'])->first();
//        dd(empty($charge));
        if (!isset($charge)|| empty($charge)) {
            $charge = new ConektaCharge();
            $charge->chargeId = $chargeData['id'];
        }
        $charge->data = $input;
        $charge->amount = $chargeData['amount'] / 100;
        $charge->currency = $chargeData['currency'];
        $charge->description = $chargeData['description'];
        $charge->statusDescription = $chargeData['status'];
        $charge->payment = $chargeData['payment_method'];
        $customerID = $chargeData['customer_id'];
        $charge->conekta_id = $customerID;
        $charge->save();
        //Logica para asignar el cargo al pago de la subscripcion
        if ($chargeData['status'] == 'Payment from order') {
            //Pago normal
        } else {
            //Voy por el Plan
//            dd($input);
//            dd($chargeData['description']);
            $plan = ConektaPlan::where('name',$chargeData['description'])->first();
//            dd($plan->planId);
            $subscription = ConektaSubscription::where('plan_id', $plan->planId)->first();
//            dd($subscription);
            $subscription->conektaCharge()->save($charge);
        }

        $userClass = $this->configConektaUser;
        $user = $userClass::where('conekta_id', $customerID)->first();
        $user->conektaCharge()->save($charge);
    }

    /**
     * Function to create entry in generallogs
     * @param type $input
     */
    function createGeneralLog($input) {
        $log = new ConektaGeneralLog();
        $log->data = $input;
        $log->eventType = $input['type'];
        $log->transactionID = $input['id'];
        $log->save();
        return $log;
    }

    public function suscriptionPayment($input, $bandera) {
        $subscriptionPayment = new ConektaSubscriptionPayment();
        $subscriptionPayment->eventType = $input['type'];
        $subscriptionPayment->transactionID = $input['id'];
        $subscriptionPayment->data = $input;
        $subscriptionData = $input['data']['object'];
        $conekta_id = $subscriptionData['customer_id'];
        $subscriptionPayment->conekta_id = $conekta_id;
        if ($bandera) {
            $subscriptionPayment->statusDescription = "EXITO";
        } else {
            $subscriptionPayment->statusDescription = "ERROR";
        }
        $userClass = $this->configConektaUser;
        $subscriptionPayment->subscription_id = $subscriptionData['id'];
        $user = $userClass::where('conekta_id', $conekta_id)->first();
        $subscriptionID = $subscriptionData['id'];
        $subscription = ConektaSubscription::where('subscriptionId', $subscriptionID)->first();
        $subscriptionPayment->save();
        $subscription->conektaSubscriptionPayment()->save($subscriptionPayment);
        $subscriptionPayment->conektaLogs()->save($this->currentLog);
        $user->conektaSubscriptionPayment()->save($subscriptionPayment);
        $this->callBackSubscriptionPayment($subscription, $bandera);
    }

    /**
     * Function that create a Plan
     * @param type $input
     */
    function createPlanModule($input) {
        //Se busca el plan por el id
        $planData = $input['data']['object'];
        $plan = ConektaPlan::where('planId', $planData['id'])->first();
        if (!isset($plan) || empty($plan)) {
            $plan = new ConektaPlan();
            $plan->planId = $planData['id'];
            $plan->name = $planData['name'];
            $plan->amount = intval($planData['amount']);
            $plan->currency = $planData['currency'];
            $plan->interval = $planData['interval'];
            $plan->frequency = $planData['frequency'];
            $plan->trial_period_days = $planData['trial_period_days'];
            $plan->expiry_count = 12;
            $plan->transactionID = "";
            $plan->status = false;
            $plan->description = "PLAN CREADO";
            $plan->descriptionConekta = "WAITING FOR WEBHOOK CREATED";
        }
        $plan->transactionID = $input['id'];
        $plan->status = true;
        $plan->description = "PLAN ACTIVO";
        $plan->descriptionConekta = "PLAN CONFIRMADO CONEKTA";
        $plan->data = $input;








        $plan->save();
    }

    function updatePlanModule($input) {
        $planData = $input['data']['object'];
        $plan = ConektaPlan::where('planId', $planData['id'])->first();
        $plan->description = "PLAN ACTUALIZADO";
        $plan->descriptionConekta = "PLAN ACTUALIZADO CONEKTA";
        $plan->save();
    }

    funCtion deletePlanModule($input) {
        $planData = $input['data']['object'];
        $plan = ConektaPlan::where('planId', $planData['id'])->first();
        $plan->description = "PLAN INACTIVO";
        $plan->descriptionConekta = "PLAN ELIMINADO CONEKTA";
        $plan->save();
    }

    /**
     * Function ti cancel a subscription
     * @param type $input
     */
    function subscriptionCanceledModule($input) {
        $subscriptionData = $input['data']['object'];
        $subscription = ConektaSubscription::where('subscriptionId', $subscriptionData['id'])->first();
        $subscription->data = $input;
        $subscription->statusDescription = $subscriptionData['status'];
        $subscription->eventType = $input['type'];
        $subscription->save();
        $this->callBackSubscriptionCancelled($subscription);
    }

    /**
     * function to change status to order when expired
     * @param type $input
     */
    function orderExpiredModule($input) {
        $orderData = $input['data']['object'];
        $order = ConektaOrder::where('orderID', $orderData['id'])->first();
        $order->data = $input;
        $order->amount = $orderData['amount'];
        $order->currency = $orderData['currency'];
        $order->statusDescription = $orderData['payment_status'];
        $order->save();
    }

    /**
     * Function to change status to order when is pending of payment
     * @param type $input
     */
    function orderPendingPaymentModule($input) {
        $orderData = $input['data']['object'];
        $order = ConektaOrder::where('orderID', $orderData['id'])->first();
        $order->data = $input;
        $order->statusDescription = $orderData['payment_status'];
        $order->amount = $orderData['amount'];
        $order->currency = $orderData['currency'];
        $order->statusDescription = $orderData['payment_status'];
        $order->save();
    }

    /**
     * function to save a order paid
     * @param type $input
     */
    function orderPaidModule($input) {
        $orderData = $input['data']['object'];
        $order = ConektaOrder::where('orderID', $orderData['id'])->first();
        $order->data = $input;
        $order->statusDescription = $orderData['payment_status'];
        $order->amount = $orderData['amount'];
        $order->currency = $orderData['currency'];
        $order->statusDescription = $orderData['payment_status'];
        $order->save();
        $this->callBackOrderPaid($order);
    }

    /**
     * function that create a order
     * @param type $input
     */
    function orderCreatedModule($input) {
        $orderData = $input['data']['object'];
        $order = ConektaOrder::where('orderID', $orderData['id'])->first();
        if (!isset($order) || empty($order)) {
            $order = new ConektaOrder();
            $order->orderID = $input['id'];
        }
        $order->data = $input;
        $order->amount = $orderData['amount'] / 100;
        $order->currency = $orderData['currency'];
        $order->statusDescription = "order.created";
        $conekta_id = $orderData['customer_info']['customer_id'];
        $order->conekta_id = $conekta_id;
        $order->save();
        $userClass = $this->configConektaUser;
        $user = $userClass::where('conekta_id', $conekta_id)->first();
        $user->conektaOrders()->save($order);
    }

    /**
     * function to charguebackcreated
     * @param type $input
     */
    function chargeChargebackCreatedModule($input) {
        $chargeData = $input['data']['object'];
        $charge = ConektaCharge::where('chargeId', $chargeData['id'])->first();
        $charge->data = $input;
        $charge->save();
    }

    /**
     * Fucntion to refunded a order
     * @param type $input
     */
    function chargeRefundedModule($input) {
        $chargeData = $input['data']['object'];
        $charge = ConektaCharge::where('chargeId', $chargeData['id'])->first();
        $charge->statusDescription = $chargeData['status'];
        $charge->data = $input;
        $charge->save();
    }

    /**
     * function to charge paid
     * @param type $input
     */
    function chargePaidModule($input) {
        $chargeData = $input['data']['object'];
        $charge = ConektaCharge::where('chargeId', $chargeData['id'])->first();
        $charge->statusDescription = $chargeData['status'];
        $charge->data = $input;
        $charge->save();
        $this->callBackChargePaid($charge);
    }

    public function callBackCreateSubscription($subscription) {
        
    }

    public function callBackSubscriptionPayment($subscriptionPayment, $bandera) {
        
    }

    public function callBackChargePaid($charge) {
        
    }

    public function callBackOrderPaid($order) {
        
    }

    public function callBackSubscriptionCancelled($subscription) {
        
    }

}
