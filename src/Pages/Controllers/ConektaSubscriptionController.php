<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Controllers;

use Webdecero\Conekta\Pages\Traits\ConektaSuscriptions;

/**
 * Description of ConektaSubscriptionController
 *
 * @author Luis
 */
class ConektaSubscriptionController {
    
    use ConektaSuscriptions;
    
    public function __construct() {
        parent::__construct();
        $this->locale = \LaravelLocalization::getCurrentLocale();
        $ContenidoGeneral = Page::where('clave', 'contenidoGeneral')->where('locale', $this->locale)->first();
        \View::share('ContenidoGeneral', $ContenidoGeneral);
    }
    
    public function viewSubscripciones(){
        return view('Pages.subscription', []);
    }
    

    public function createSubscriptionModule(Request $request){
        $input = $request->all();
        //Se procesa input para obtener el $arrayData
        $idUserConekta = $input['idUser'];
        $user = User::findOrFail($idUserConekta);
        $configConekta = config('manager.conekta.config');
        $idPlan = $configConekta['conekta_planIdDefault'];
        $arrayData = array(
            'plan' => $idPlan
        );
        $response = $this->createSubscription($arrayData, $idUserConekta);
        if ($response['success']) {
            $user->status = TRUE;
            $user->registerDescription = "USUARIO_ACTIVO";
            $user->usedTrial = true;
            $user->save();
            $mail = config('mail');
            //Validacion para el trial
            
            if ($user->canceled == 1) {
                // Se envia el mensaje de gracias por reactivar la cuenta
                \Mail::to($user->email)->send(new \App\Mail\UserReActivated($user));
            } else { // Se envia el mensaje con 30 dias de prueba
                \Mail::to($user->email)->send(new \App\Mail\UserActivated($user));
            }
            //Se crea la tabla de la subscripcion
             
            $subscription = new ConektaSubscription();
            $subscription->firstData = $response;
            $subscription->subscriptionId = $response['data']['id'];
            $subscription->statusDescription = $response['data']['status'];
            $subscription->descriptionConekta = "WAITING FOR CONEKTA ...";
            $subscription->save();
            
            return back()->with([
                        'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
            ]);
        } else {
            return back()->with([
                        'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
            ]);
        }
    }
    
    
   
    
    
}
