<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Controllers;

/**
 * Description of WebhookInterface
 *
 * @author Luis
 */
interface WebhookInterface {

    public function callBackCreateSubscription($subscription);

    public function callBackSubscriptionPayment($subscriptionPayment,$bandera);
    
    public function callBackChargePaid($charge);
    
    public function callBackOrderPaid($order);
    
    public function callBackSubscriptionCancelled($subscription);
    
    

//    public function suscriptionPayment($input);

//    public function suscriptionPaymentFailed($input);

//    public function chargeCreated($input);
}
