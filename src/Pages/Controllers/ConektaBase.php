<?php

namespace Webdecero\Conekta\Pages\Controllers;

use Webdecero\Base\Pages\Controllers\PagesController as WebdeceroPageController;
use Conekta\Conekta as Conekta;

class ConektaBase extends WebdeceroPageController {

   
    
    protected $configConekta;

    public function __construct() {

        parent::__construct();
        // setup Conekta api context
        $this->configConekta = config('manager.conekta.config');
        Conekta::setApiKey($this->configConekta['api_key']);
        Conekta::setApiVersion($this->configConekta['api_version']);
    }

}
