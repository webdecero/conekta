<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Traits;

use \Conekta\Order as Order;
use \Conekta\Handler as Handler;

/**
 * Description of ConektaCharges
 *
 * @author Luis
 */
trait ConektaCharges {

    /**
     * function to create a charge in any order
     * @param type $arrayData
     * array(
            'payment_method' => array(
                'type'       => 'oxxo_cash',
                'expires_at' => 1501027200
            )
        )
     * @param type $idOrder
     * @return $response: response function
     */
    
    public  function createCharge($arrayData, $idOrder) {
        $response = [];
        try {
            $order = Order::find($idOrder);
            $charge = $order->createCharge($arrayData);
            $response['success'] = true;
            $response['data'] = $charge;
            $response['message'] = trans('ConektaLang::conekta.charge.create');
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

}
