<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Traits;

use \Conekta\Order as Order;
use \Conekta\Handler as Handler;

/**
 * Description of ConektaOrders
 *
 * @author Luis
 */
trait ConektaOrders {

    /**
     * function to create order
     * @param type $arrayData
     * array(
      'currency' => 'MXN',
      'customer_info' => array(
      'customer_id' => 'cus_zzmjKsnM9oacyCwV3'
      ),
      'line_items' => array(
      array(
      'name' => 'Box of Cohiba S1s',
      'unit_price' => 35000,
      'quantity' => 1
      )
      ),
      'charges' => array(
      array(
      'payment_method' => array(
      'type' => 'default'
      )
      )
      )
      )
     * @return array
     */
    public function createOrder($arrayData) {
        $response = [];
        try {
            $order = Order::create($arrayData);
            $response['success'] = true;
            $response['data'] = $order;
            $response['message'] = trans('ConektaLang::conekta.order.create', [$order->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

    /**
     * function to update order
     * @param type $arrayData
     * array(
      'currency' => 'MXN'
      ));
     * @param type $idOrderConekta
     * @return $response: response function
     */
    public function updateOrder($arrayData, $idOrderConekta) {
        $response = [];
        try {
            $order = Order::find($idOrderConekta);
            $order->update($arrayData);
            $response['success'] = true;
            $response['data'] = $order;
            $response['message'] = trans('ConektaLang::conekta.order.update', [$order->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

    /**
     * function to capture a order
     * @param type $idOrderConekta
     * @return type
     */
    public function captureOrder($idOrderConekta) {
        $response = [];
        try {
            $order = Order::find($idOrderConekta);
            $order->capture();
            $response['success'] = true;
            $response['data'] = $order;
            $response['message'] = trans('ConektaLang::conekta.order.capture', [$order->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * function to refound order
     * @param type $arrayData
     * array(
                'reason' => 'requested_by_client',
                'amount' => 15000
            )
     * @param type $idOrderConekta
     * @return type
     */
    public function refoundOrder($arrayData, $idOrderConekta) {
        $response = [];
        try {
            $order = Order::find($idOrderConekta);
            $order->refund($arrayData);
            $response['success'] = true;
            $response['data'] = $order;
            $response['message'] = trans('ConektaLang::conekta.order.refound', [$order->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
}
