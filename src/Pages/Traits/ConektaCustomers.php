<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Traits;


use \Conekta\Customer as Customer;
use \Conekta\Handler as Handler;

/**
 * Description of ConektaPlans
 *
 * @author Luis
 */
trait ConektaCustomers {

    /**
     * Function to createa customer
     *  array(
     *   'name'  => "Mario Perez",
     *   'email' => "usuario@example.com",
     *   'phone' => "+5215555555555",
     *   'payment_sources' => array(array(
     *       'token_id' => "tok_test_visa_4242",
     *       'type' => "card"
     *   )),
     *   'shipping_contacts' => array(array(
     *     'phone' => "+5215555555555",
     *     'receiver' => "Marvin Fuller",
     *     'address' => array(
     *       'street1' => "Nuevo Leon 4",
     *       'street2' => "fake street",
     *       'country' => "MX",
     *       'postal_code' => "06100"
     *     )
     *   ))
     * )
     * @param type $arrayData
     * @return type
     */
    public function createCustomer($arrayData) {
        $response = [];
        try {
            $customer = Customer::create($arrayData);
            $response['success'] = true;
            $response['data'] = $customer;
            $response['message'] = trans('ConektaLang::conekta.customer.create', [$customer->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

    /**
     * function to update a customer
     * @param type $arrayData
     * array(
     *  'name' => "Mario Perez",
     *  'email' => 'usuario@example.com',
     * )
     * @param type $idUserConekta
     * @return $response: response function
     */
    
    public function updateCustomer($arrayData, $idUserConekta) {
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $customer->update($arrayData);
            $response['success'] = true;
            $response['data'] = $customer;
            $response['message'] = trans('ConektaLang::conekta.customer.update', [$customer->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * function to delete a customer
     * @param type $idUserConekta
     * @return $response: response function
     */
    
    public function deleteCustomer($idUserConekta){
        $response = [];
        try{
            $customer = Customer::find($idUserConekta);
            $customer->delete();
            $response['success'] = true;
            $response['data'] = $customer;
            $response['message'] = trans('ConektaLang::conekta.customer.delete', [$customer->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    
     /**
     * function to add payment source
     * @param type $arrayData
     * array(
         'token_id' => $tokenId,
         'type' => 'card'
      )
     * @param type $idUserConekta
     * @return type
     */
    public function addPaymentSource($arrayData,$idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $source = $customer->createPaymentSource($arrayData);
            $response['success'] = true;
            $response['data'] = $source;
            $response['message'] = trans('ConektaLang::conekta.customer.createPayment', [$source->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    
    /**
     * function to update payment source
     * @param type $arrayData
     * array(
         'name' => $name,
         'type' => 'card'
      )
     * @param type $idUserConekta
     * @return type
     */
    public function updatePaymentSource($arrayData,$idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            
            $source = $customer->payment_sources[0]->update($arrayData);
            $response['success'] = true;
            $response['data'] = $source;
            $response['message'] = trans('ConektaLang::conekta.customer.updatePayment', [$source->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * function to remove payment source
     * @param type $idUserConekta
     * @return type
     */
    public function removePaymentSource($idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $source = $customer->payment_sources[0]->delete();
            $response['success'] = true;
            $response['data'] = $source;
            $response['message'] = trans('ConektaLang::conekta.customer.removePayment', [$source->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

}
