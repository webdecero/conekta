<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Traits;

use \Conekta\Plan as Plan;
use \Conekta\Handler as Handler;

/**
 * Description of ConektaPlans
 *
 * @author Luis
 */
trait ConektaPlans {

    /**
     * Function to create a plan
     * @param type $arrayData
     * array(
     *  'id' => {idPlan},
     *  'name' => "{namePlan}",
     *  'amount' => {amountPlan} in cents, example: 300,
     *  'currency' => "{corruency}" iso value 3 charactrers, examples: USD, MXN,
     *  'interval' => "{intervalPlan}" interval plan, examples: week, half_month, month, year,
     *  'frequency' => {frecuencyOfCharges} number of charges, example: 1,
     *  'trial_period_days' => {trialPeriod}, days of a trial, example: 30,
     *  'expiry_count' => {ExpiredCounts}, max number to charges after expired suscription, example: 12
     * )
     * @param type $idUserConekta
     * @return $response: response function
     */
    public function createPlan($arrayData) {
        $response = [];
        try {
            $plan = Plan::create($arrayData);
            $response['success'] = true;
            $response['data'] = $plan;
            $response['message'] = trans('ConektaLang::conekta.plan.create', [$plan->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getConektaMessage();
        }
        return $response;
    }

    /**
     * Function to update a plan
     * @param type $arrayData
     * array(
     *  'id' => "{idPlan}" this value cant update,
     *  'name' => "{namePlan} name of plan"
     *  'amount' => {amountPlan} amount of plan in cents
     * )
     * @param type $idPlanConekta
     * @return $response: response function
     */
    public function updatePlan($arrayData, $idPlanConekta) {
        $response = [];
        try {
            $plan = Plan::find($idPlanConekta);
            $plan->update($arrayData);
            $response['success'] = true;
            $response['data'] = $plan;
            $response['message'] = trans('ConektaLang::conekta.plan.update', [$plan->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getConektaMessage();
        }
        return $response;
    }

    /**
     * Fucntion to delete a plan
     * @param type $idPlanConekta
     * @return $response: response function
     */
    public function deletePlan($idPlanConekta) {
        $response = [];
        try {
            $plan = Plan::find($idPlanConekta);
            $plan->delete();
            $response['success'] = true;
            $response['data'] = $plan;

            $response['message'] = trans('ConektaLang::conekta.plan.delete', [$plan->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;
            
            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

}
