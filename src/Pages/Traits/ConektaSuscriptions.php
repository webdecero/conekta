<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Pages\Traits;

use \Conekta\Customer as Customer;
use \Conekta\Handler as Handler;

/**
 * Description of ConektaPlans
 *
 * @author Luis
 */

trait ConektaSuscriptions {
    
    /**
     * Function to create a subscription
     * @param type $arrayData
     * array(
     *  'plan' => '{idConektaPlan}'
     * )
     * @param type $idUserConekta
     * @return $response: function response
     */
    
    public function createSubscription($arrayData, $idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $subscription = $customer->createSubscription($arrayData);
            $response['success'] = true;
            $response['data'] = $subscription;
            $response['message'] = trans('ConektaLang::conekta.subscription.create', [$subscription->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;

            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * Function to update a Subscription
     * @param type $arrayData
     * array(
     *  'plan' => '{idConektaPlan}'
     * )
     * @param type $idUserConekta
     * @return $response: function response
     */
    
    public function updateSubscription($arrayData,$idUserConekta){
        $response = [];
        try{
            $customer = Customer::find($idUserConekta);
            $subscription = $customer->subscription->update($arrayData);
            $response['success'] = true;
            $response['data'] = $subscription;
            $response['message'] = trans('ConektaLang::conekta.subscription.update', [$subscription->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;

            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * Function to pause a subscription
     * @param type $idUserConekta
     * @return $response: function response
     */
    
    public function pauseSubscription($idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $subscription = $customer->subscription->pause();
            $response['success'] = true;
            $response['data'] = $subscription;
            $response['message'] = trans('ConektaLang::conekta.subscription.pause', [$subscription->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;

            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * Funciton to resume a subscription
     * @param type $idUserConekta
     * @return $response: function response
     */
    public function resumeSubscription($idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $subscription = $customer->subscription->resume();
            $response['success'] = true;
            $response['data'] = $subscription;
            $response['message'] = trans('ConektaLang::conekta.subscription.resume', [$subscription->id]);
       } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;

            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }
    
    /**
     * Function to cancel a subscription
     * @param type $idUserConekta
     * @return $response: function response
     */
    public function cancelSubscription($idUserConekta){
        $response = [];
        try {
            $customer = Customer::find($idUserConekta);
            $subscription = $customer->subscription->cancel();
            $response['success'] = true;
            $response['data'] = $subscription;
            $response['message'] = trans('ConektaLang::conekta.subscription.cancel', [$subscription->id]);
        } catch (Exception $ex) {
            $response['success'] = false;
            $response['message'] = $ex->getMesage();
        } catch (Handler $ex) {
            $response['success'] = false;

            $response['conektaError'] = $ex->getConektaMessage();
        }
        return $response;
    }

}
