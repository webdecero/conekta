<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Webdecero\Conekta\Pages\Traits;
/**
 * Description of ConektaRelations
 *
 * @author Luis
 */
trait ConektaRelations {
    
    public function conektaOrders() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaOrder::class);
    }

    public function conektaSubscription() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscription::class);
    }
    
    public function conektaGeneralLogs(){
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaGeneralLog::class);
    }
    
    public function conektaCharge(){
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaCharge::class);
    }
    
    public function conektaSubscriptionPayment(){
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscriptionPayment::class);
    }
    
    public function conektaSubscriptionPaymentFailed(){
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscriptionPaymentFailed::class);
    }
    
    
}
