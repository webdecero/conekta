<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    'failed' => 'Ocurrió un error al realizar el pago<br> No se realizó ningún cargo a su tarjeta.<br> Favor de intentar con otra tarjeta.<br>',
    'denied' => 'Cobro declinado<br>No se realizó ningún cargo a su tarjeta.<br>La operación fue declinada por su banco emisor.<br>Favor de intentar con otra tarjeta.',
    'approved' => '<h3>Gracias por tu donativo.</h3><p> Recibe más información sobre cómo con tu ayuda, elevamos la movilidad social en México. Regístrate y conoce más.</p>',
    'noRespuesta' => 'Ocurrió un error al realizar el pago.',
    'expired' => 'Ha caducado la página, es necesario pulsar el botón comprar nuevamente.',
    'canceled' => 'Operación Cancelada<br>No se realizó ningún cargo a su tarjeta.',
    'utilizado' => 'La referencia ha sido ocupada,  es necesario pulsar el botón comprar nuevamente.',
    'comprado' => 'El usuario ya cuenta con una compra realizada.',
    'vacio' => 'El carrito se enecuentra vacio',
    'incomplete' => 'Operación incompleta intentar nuevamente.',
    
    
    
    'customer' => array(
        'create' => 'Usuario creado correctamente ',
        'update' => 'Usuario actualizado correctamente ',
        'delete' => 'Usuario borrado correctamente ',
        'createPayment' => 'Metodo de pago creado correctamente ',
        'updatePayment' => 'Metodo de pago actualizado correctamente ',
        'removePayment' => 'Metodo de pago borrado correctamente ',
        
        
        
    ),
    
    
 
    
    'order' => array(
        'create' => 'Orden creado correctamente ',
        'update' => 'Orden actualizada correctamente ',
        'capture' => 'Orden procesada correctamente ',
        'refound' => 'Orden reembolsada correctamente ',
        
        
        
        
    ),
    
       
    'subscription' => array(
        'create' => 'Suscripción creadacorrectamente ',
        'update' => 'Suscripción actualizada correctamente ',
        'pause' => 'Suscripción pausada correctamente ',
        'resume' => 'Suscripción reanuda correctamente ',
        'cancel' => 'Suscripción cancelada correctamente ',
        
    ),
);
