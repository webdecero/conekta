@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Log panel')
@section('formAction', route('manager.conekta.log.dataTable'))



@section('inputs')




<div class="form-group">
	<label for="eventType">Evento</label> <br>

	<select class="reloadTable" name="eventType">
                <option value="">Ninguno</option>
		<option value="subscription.created">Subscripción creada</option>
                <option value="subscription.paid">Subscripción pagada</option>
                <option value="subscription.canceled">Subscripción cancelada</option>
                <option value="subscription.payment_failed">Pago fallido de la subscripción</option>
                <option value="charge.created">Cargo creado</option>
                <option value="charge.paid">Cargo pagado</option>
                <option value="charge.refunded">Cargo devuelto</option>
                <option value="charge.chargeback.created">Devolucion del cargo creada</option>
                <option value="order.created">Orden creada</option>
                <option value="order.paid">Orden pagada</option>
                <option value="order.pending_payment">Orden pendiente de pago</option>
                <option value="order.expired">Orden expirada</option>
                <option value="plan.created">Plan creado</option>
                <option value="plan.updated">Plan actualizado</option>
                <option value="plan.deleted">Plan eliminado</option>
	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">


	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>


@endsection



@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="_id" >
	<strong>Id</strong>
</th>

<th data-name="eventType" >
	<strong>Evento</strong>
</th>



<th data-name="transactionID"  data-orderable="true"  data-order="asc" >
	<strong>Identificador de la transacción</strong>
</th>

<th data-name="data"  data-orderable="true"  data-order="asc" >
	<strong>Información</strong>
</th>



<th data-name="created_at"><strong>Fecha Alta</strong></th>

<!--<th data-name="opciones">
	<strong>Opciones</strong>
</th>-->




@stop
