@extends('baseViews::layouts.dataTable')

@section('sectionAction', 'Tabla')
@section('sectionName', 'Order Conekta')
@section('formAction', route('manager.conekta.order.dataTable'))


@section('inputs')

<div class="form-group">
	<label for="statusDescription">Estatus</label> <br>

	<select class="reloadTable" name="statusDescription">
                <option value="">Ninguno</option>
		<option value="pending_payment">Pendiente de pago</option>
		<option value="declined">Declinado</option>
		<option value="expired">Expirada</option>
                <option value="paid">Pagada</option>
                <option value="refunded">Devuelta</option>
                <option value="partially_refunded">Parcialmente Devuelta</option>
                <option value="charged_back">PAgada de vuelta</option>
                <option value="pre_authorized">Preautorizado</option>
                <option value="voided">Vacia</option>


	</select>
</div>

<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">
	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >
			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL
		</button>
	</div>
</div>


@endsection



@section('dataTableColums')


<th data-details="true" ></th>
<th data-name="_id" >
	<strong>Id</strong>
</th>
<th data-name="orderID" >
	<strong>Identificador de la orden</strong>
</th>
<th data-name="amount"  data-orderable="true"  data-order="asc" >
	<strong>Monto</strong>
</th>
<th data-name="currency"  data-orderable="true"  data-order="asc" >
	<strong>Moneda</strong>
</th>
<th data-name="statusDescription" data-orderable="true" >
	<strong>Estatus</strong>
</th>
<th data-name="conekta_id" data-orderable="true" >
	<strong>Nombre del cliente</strong>
</th>
<th data-name="created_at"><strong>Fecha Alta</strong></th>
@stop
