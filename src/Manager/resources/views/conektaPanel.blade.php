@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Conekta pagos')
@section('formAction', route('manager.conekta.dataTable'))


@section('inputs')


<!--<div class="form-group">
	<label for="estatus">Estatus</label> <br>

	<select class="reloadTable" name="response">
		<option value="">Ninguno</option>
		<option value="approved">Aprobado</option>
		<option value="canceled">Cancelado</option>
		<option value="denied">Declinado</option>
		<option value="failed">Fallido</option>


	</select>
</div>-->


<div class="form-group">
	<label for="type">Tipo</label> <br>

	<select class="reloadTable" name="type">
		<option value="">Ninguno</option>
		<option value="Plan">Plan</option>
		<option value="Pay">Pago Único</option>


	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">


	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>


@endsection



@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="_id" >
	<strong>Id</strong>
</th>

<th data-name="conekta_id" >
	<strong>IdUsuario Conekta</strong>
</th>

<!--<th data-name="usuario" >
	<strong>Usuario</strong>
</th>-->

<th data-name="eventType" >
	<strong>Evento</strong>
</th>


<th data-name="transactionID"  data-orderable="true"  data-order="asc" >
	<strong>Referencia</strong>
</th>

<th data-name="created_at"><strong>Fecha Alta</strong></th>


@stop
