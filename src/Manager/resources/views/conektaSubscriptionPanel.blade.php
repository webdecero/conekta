@extends('baseViews::layouts.dataTable')

@section('sectionAction', 'Tabla')
@section('sectionName', 'Suscripciones Conekta')
@section('formAction', route('manager.conekta.subscription.dataTable'))


@section('inputs')

<div class="form-group">
	<label for="statusDescription">Estatus</label> <br>
	<select class="reloadTable" name="statusDescription">
		<option value="">Ninguno</option>
		<option value="in_trial">En trial</option>
		<option value="active">Activa</option>
                <option value="past_due">Vencida</option>
                <option value="paused">En Pausa</option>
                <option value="canceled">Cancelada</option>
	</select>
</div>

<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">
	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >
			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL
		</button>
	</div>
</div>

@endsection

@section('dataTableColums')


<th data-details="true" ></th>
<th data-name="_id" >
	<strong>Id</strong>
</th>
<th data-name="subscriptionId" >
	<strong>Identificador de la suscripción</strong>
</th>
<th data-name="eventType"  data-orderable="true"  data-order="asc" >
	<strong>Evento</strong>
</th>
<th data-name="statusDescription" data-orderable="true">
	<strong>Estatus</strong>
</th>
<th data-name="plan_id" data-orderable="true" >
	<strong>Identificador del Plan</strong>
</th>
<th data-name="conekta_id" >
	<strong>Nombre del Cliente</strong>
</th>
<th data-name="user_id" data-orderable="true">
	<strong>Identificador del Cliente</strong>
</th>
<!--<th data-name="history" >
	<strong>Historial</strong>
</th>-->
<th data-name="created_at"><strong>Fecha Alta</strong></th>
<th data-name="charges"><strong>Cargos</strong></th>

<th data-name="descriptionConekta"><strong>Cancelar</strong></th>

@stop
