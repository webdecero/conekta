@extends('baseViews::layouts.dataTable')

@section('sectionAction', 'Tabla')
@section('sectionName', 'Planes Conekta')
@section('formAction', route('manager.conekta.plan.dataTable'))


@section('inputs')

<div class="form-group">
	<label for="currency">Moneda</label> <br>
	<select class="reloadTable" name="currency">
		<option value="">Ninguno</option>
		<option value="MXN">Pesos</option>
		<option value="USD">Dolares</option>
	</select>
</div>

<div class="form-group">
	<label for="interval">Intervalo de tiempo</label> <br>
	<select class="reloadTable" name="interval">
		<option value="">Ninguno</option>
		<option value="week">Semanal</option>
		<option value="half_month">Quincenal</option>
		<option value="month">Mensual</option>
                <option value="year">Anual</option>
	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">
    
        <div class="btn-group" role="group" aria-label="...">
		<a class="btn btn-success " href="{{ route('manager.conekta.plan.create')}}" >
			<i class="fa fa-plus-circle"></i> NUEVO
		</a>
	</div>


	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>


@endsection



@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="_id" >
	<strong>Id</strong>
</th>

<th data-name="planId" >
	<strong>Identificador del plan</strong>
</th>



<th data-name="name"  data-orderable="true"  data-order="asc" >
	<strong>Nombre</strong>
</th>



<th data-name="amount"  data-orderable="true"  data-order="asc" >
	<strong>Cantidad</strong>
</th>


<th data-name="currency" data-orderable="true">
	<strong>Moneda</strong>
</th>


<th data-name="interval" data-orderable="true" >
	<strong>Intervalo de tiempo</strong>
</th>


<th data-name="description" >
	<strong>Descripción</strong>
</th>

<th data-name="descriptionConekta" >
	<strong>Estatus Conekta</strong>
</th>

<th data-name="created_at"><strong>Fecha Alta</strong></th>
<th data-name="opciones"><strong>Opciones</strong></th>


@stop
