@extends('baseViews::layouts.dataTable')

@section('sectionAction', 'Tabla')
@section('sectionName', 'Cargos Conekta')
@section('formAction', route('manager.conekta.charge.dataTable'))


@section('inputs')

<div class="form-group">
    <label for="statusDescription">Estatus</label> <br>
    <select class="reloadTable" name="statusDescription">
        <option value="">Ninguno</option>
        <option value="pending_payment">Pendiente</option>
        <option value="paid">Pagado</option>
    </select>
</div>

<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">
    <div class="btn-group" role="group" aria-label="...">
        <button type="submit"class="btn btn-success " >
            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL
        </button>
    </div>
</div>


@endsection

@section('dataTableColums')

<th data-details="true" ></th>
<th data-name="_id" >
    <strong>Id</strong>
</th>
<th data-name="chargeId" >
    <strong>Identificador del cargo</strong>
</th>
<th data-name="amount"  data-orderable="true"  data-order="asc" >
    <strong>Monto</strong>
</th>
<th data-name="currency"  data-orderable="true"  data-order="asc" >
    <strong>Moneda</strong>
</th>
<th data-name="description" data-orderable="true">
    <strong>Descripcion</strong>
</th>
<th data-name="statusDescription" data-orderable="true" >
    <strong>Estatus</strong>
</th>
<th data-name="type" >
    <strong>Tipo</strong>
</th>
<!--<th data-name="plan_id" data-orderable="true" >
	<strong>Identificador del Plan</strong>
</th>-->
<th data-name="conekta_id" data-orderable="true">
	<strong>Nombre del Ciente</strong>
</th>
<th data-name="user_id" data-orderable="true">
	<strong>Identificador del Ciente</strong>
</th>
<th data-name="subscription" data-orderable="true">
	<strong>Suscripción</strong>
</th>
<th data-name="created_at"><strong>Fecha Alta</strong></th>
<!--<th data-name="opciones"><strong>Opciones</strong></th>-->

@stop
