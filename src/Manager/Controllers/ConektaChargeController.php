<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\ManagerController;
use Auth;
use Validator;
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;

/**
 * Description of ConektaChargeController
 *
 * @author Luis
 */
class ConektaChargeController extends ManagerController{
   use DynamicInputs;

    protected $configPages = '';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['user'] = Auth::user();
        return view('conektaViews::conektaCharge', $this->data);
    }
}
