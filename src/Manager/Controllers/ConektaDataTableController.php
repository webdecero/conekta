<?php

namespace Webdecero\Conekta\Manager\Controllers;

//Models

use Webdecero\Conekta\Manager\Models\Conekta;
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;

class ConektaDataTableController extends DataTableController {

    public function __construct() {


        $configConekta = config('pages.conekta');

        $this->isLive = ($configConekta['settings']['mode'] == 'live') ? true : false;
    }

    protected $collectionName = 'conekta';
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'reference' => 'contains',
            'paymentId' => 'contains',
            'referenceAgreementId' => 'contains',
            'title' => 'contains',
            'user_id' => 'contains',
            'payerEmail' => 'contains',
            'payerId' => 'contains'
        ],
        'response' => 'equal',
        'type' => 'equal',
    ];
    protected $fieldsDetails = [
        '_id' => 'ID',
        'conekta_id' => 'Id de usuario en conekta',
        'eventType' => 'Evento',
        'transactionID' => 'Referencia',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
       '_id' => 'ID',
        'conekta_id' => 'Id de usuario en conekta',
        'eventType' => 'Evento',
        'transactionID' => 'Referencia',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);
        
        if($field == 'eventType'){
            switch ($item['eventType']){
                case 'order.paid':
                    $record = "Orden Pagada";
                    break;
                case 'subscription.paid':
                    $record = "Suscripcion Pagada";
                    break;
                case 'subscription.payment_failed':
                    $record = "Pago de la suscripcion fallido";
                    break;
                case 'charge.paid':
                    $record = "Cargo pagado";
                    break;
                case 'order.created':
                    $record = "Orden creada";
                    break;
                case 'customer.created':
                    $record = "Cliente Creado";
                break;
                case 'charge.created':
                    $record = "Cargo Creado";
                break;
            }
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {


        $record = $this->formatRecord($field, $item);


        if ($field == 'user') {

            $pago = Paypal::find((string) $item['_id']);

            $record = isset($pago->user->email) ? $pago->user->email : 'ninguno';
        } else if ($field == 'payerId') {


            $record = isset($item['payerId']) ? $item['payerId'] : 'ninguno';
        }





        return $record;
    }

}
