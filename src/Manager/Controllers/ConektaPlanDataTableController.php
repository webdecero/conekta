<?php

namespace Webdecero\Conekta\Manager\Controllers;

//Models
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Conekta\Manager\Models\ConektaPlan;
use Webdecero\Conekta\Manager\Models\ConektaSubscription;

class ConektaPlanDataTableController extends DataTableController {

    protected $collectionName = 'conektaPlan';
    protected $searchable = [
        'query' => [
            'planId' => 'contains',
            'name' => 'contains',
            'description' => 'contains',
        ],
        'currency' => 'contains',
        'interval' => 'contains',
    ];
    protected $fieldsDetails = [
        '_id' => 'Id',
        'planId' => 'Identificador del plan',
        'name' => 'Nombre del plan',
        'amount' => 'Monto del plan',
        'currency' => 'Moneda',
        'interval' => 'Intervalo de tiempo',
        'description' => 'Descripción',
        'transactionID' => 'Número de transacción',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'Id',
        'planId' => 'Identificador del plan',
        'name' => 'Nombre del plan',
        'amount' => 'Monto del plan',
        'currency' => 'Moneda',
        'interval' => 'Intervalo de tiempo',
        'description' => 'Descripción',
        'transactionID' => 'Número de transacción',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        if ($field == 'amount') {
            $conversion = intval($item['amount'])/100;
            $record = '$' . number_format($conversion, 0, '.', ',');
        }else if ($field == 'url_details') {
            $record = route('manager.conekta.plan.dataTable', ['id' => $item['_id']]);
        }else if ($field == 'opciones') {
            // Se valida que un plan no tenga subscripciones
            $subscripciones = ConektaSubscription::where('conekta_plan_id',(string) $item['_id'])->first();
            if($item['status'] && !isset($subscripciones)){
                $record = $record. "<a href=" . route('manager.conekta.plan.destroy', ['id' => $item['_id']]) . "  class='text-center center-block borrar' data-toggle='tooltip' title='Eliminar' > <i class='fa fa-trash-o'></i> </a>";
            }else{
                $record = "";
            }
//            $record = "<a href=" . route('manager.conekta.plan.edit', ['id' => $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";
        }else if($field == 'interval'){
            $value = $item['interval'];
            switch ($value) {
                case 'week':
                    $record = "Semanal";
                    break;
                case 'half_month':
                    $record = "Quincenal";
                    break;
                case 'month':
                    $record = "Mensual";
                    break;
                case 'year':
                    $record = "Anual";
                    break;
                case 'minute':
                    $record = "Por minuto";
                default:
                    break;
            }
        }
        return $record;
    }

}
