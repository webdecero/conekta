<?php

namespace Webdecero\Conekta\Manager\Controllers;

//Providers
use Auth;
use Validator;
//Models
//Helpers and Class
use Webdecero\Conekta\Manager\Controllers\ConektaBaseManager;
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;
use Webdecero\Conekta\Pages\Traits\ConektaPlans;
use Webdecero\Conekta\Manager\Models\ConektaPlan;

class ConektaPlanController extends ConektaBaseManager {

    use ConektaPlans;
    use DynamicInputs;

    protected $configPages = '';
    protected $returnRoute = 'manager.conekta.plan.index';

    public function __construct() {
        parent::__construct();
        $this->configPages = config('manager.conekta.conektaPlan');
    }

    public function index() {
        $this->data['user'] = Auth::user();
        return view('conektaViews::conektaPlanPanel', $this->data);
    }

    public function create() {
        $this->data['dataPage'] = [];
        $type = request('type', 'index');
        $configPage = $this->configPages[$type];
        $processData = $this->processPageUI($configPage, $this->data);
        return view('conektaViews::conektaPlanAddForm', $processData);
    }

    public function store(Request $request) {
        $input = $request->all();
        $rules = array(
            'name' => array('required'),
            'currency' => array('required'),
            'amount' => array('required'),
            'interval' => array('required'),
            'frequency' => array('required'),
            'trialDays' => array('required'),
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('conektaLang::conekta.mensajes.registroIncompleto'),
                    ])->withInput($request->except('password'));
        }

        // Se guarda en la bd el plan
        $id = mb_strtolower(str_random(6), 'UTF-8');
        $plan = new ConektaPlan();
        $plan->planId = $id;
        $plan->name = $input['name'];
        $plan->amount = intval($input['amount']) * 100;
        $plan->currency = $input['currency'];
        $plan->interval = $input['frequency'];
        $plan->frequency = $input['interval'];
        $plan->trial_period_days = $input['trialDays'];
        $plan->expiry_count = 12;
        $plan->transactionID = "";
        $plan->status = false;
        $plan->description = "PLAN CREADO";
        $plan->descriptionConekta = "WAITING FOR WEBHOOK CREATED";

        $data = array(
            'id' => $id,
            'name' => $input['name'],
            'amount' => intval($input['amount']) * 100,
            'currency' => $input['currency'],
            'interval' => $input['frequency'],
            'frequency' => $input['interval'],
            'trial_period_days' => $input['trialDays'],
            'expiry_count' => 12
        );

        $response = $this->createPlan($data);
        if ($response['success']) {
            $plan->dataSendCreate = $response['data'];
            $plan->save();
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }

    public function edit($id) {
        $isValid = Utilities::idMongoValid($id);
        $record = ConektaPlan::findOrFail($id);
        if (!$isValid || !isset($record->id)) {
            return back()->with([
                        'error' => trans('ConektaLang::conekta.mensajes.registroNoEncontrado'),
            ]);
        }
        $type = request('type', 'index');
        $record->amount = $record->amount / 100;
        $this->data['record'] = $record;
        $this->data['dataPage'] = $record->toArray();
        $configPage = $this->configPages['editPlan'];
        $processData = $this->processPageUI($configPage, $this->data);
        return view('conektaViews::conektaPlanEditForm', $processData);
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $rules = array(
            'amount' => array('required'),
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('ConektaLang::conekta.mensajes.registroIncompleto'),
            ]);
        } else {
            $isValid = Utilities::idMongoValid($id);
            $plan = ConektaPlan::findOrFail($id);
            if (!$isValid || !isset($plan->planId)) {
                return back()->with([
                            'error' => trans('ConektaLang::conekta.mensajes.registroNoEncontrado'),
                ]);
            }
            $amount = intval($input['amount']) * 100;
            $data = array(
                'amount' => $amount,
            );
            $response = $this->updatePlan($data, $plan->planId);
            if ($response['success']) {
                $plan->amount = $amount;
                $plan->description = "PLAN ACTUALIZADO";
                $plan->descriptionConekta = "WAITING FOR WEBHOOK UPDATE";
                $plan->save();
                return redirect()->route($this->returnRoute)->with([
                            'mensaje' => $response['message'],
                ]);
            } else {
                $whitError = $this->getMessageError($response);
                return back()->with($whitError);
            }
        }
    }

    public function destroy($id) {
        $isValid = Utilities::idMongoValid($id);
        $plan = ConektaPlan::findOrFail($id);
        if (!$isValid || !isset($plan->planId)) {
            return back()->with([
                        'error' => trans('ConektaLang::conekta.mensajes.registroNoEncontrado'),
            ]);
        }
        $response = $this->deletePlan($plan->planId);
        if ($response['success']) {
            $plan->description = "PLAN ELIMINADO";
            $plan->descriptionConekta = "WAITING FOR WEBHOOK DELETE";
            $plan->status = false;
            $plan->save();
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }

}
