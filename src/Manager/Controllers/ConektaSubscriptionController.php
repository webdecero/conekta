<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\ManagerController;
use Illuminate\Http\Request;
use Webdecero\Conekta\Pages\Traits\ConektaSuscriptions;

/**
 * Description of ConektaSubscriptionController
 *
 * @author Luis
 */
class ConektaSubscriptionController extends ManagerController {

    use ConektaSuscriptions;

    public function index() {
        return view('conektaViews::conektaSubscriptionPanel', $this->data);
    }

    public function pauseSubscriptionModule($idUserConekta){
        $response = $this->pauseSubscription($idUserConekta);
        if ($response['success']) {
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }
    
    public function resumeSubscriptionModule($idUserConekta){
        $response = $this->resumeSubscription($idUserConekta);
        if ($response['success']) {
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }
    
    public function cancelSubscriptionModule($idUserConekta){
        $response = $this->cancelSubscription($idUserConekta);
        if ($response['success']) {
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }
    
    public function createSubscriptionModule($idUserConeta){
        $response = $this->createSubscription($idUserConeta);
        if ($response['success']) {
            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => $response['message'],
            ]);
        } else {
            $whitError = $this->getMessageError($response);
            return back()->with($whitError);
        }
    }
    
    
}
