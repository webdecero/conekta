<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Conekta\Manager\Models\ConektaOrder;

/**
 * Description of ConektaOrderDataTableController
 *
 * @author Luis
 */
class ConektaLogDataTableController extends DataTableController {

    protected $collectionName = 'conektaLog';
    protected $searchable = [
        'query' => [
            'eventType' => 'contains',
        ],
        'eventType' => 'contains',
    ];
    protected $fieldsDetails = [
        '_id' => 'Id',
        'eventType' => 'Evento',
        'transactionID' => 'Identificador de la transacción',
        'data' => 'Información',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'Id',
        'eventType' => 'Evento',
        'transactionID' => 'Identificador de la transacción',
        'data' => 'Información',
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        if ($field == 'eventType') {
            $valor = $item['eventType'];
            switch ($valor) {
                case 'subscription.created':
                    $record = "Subscripción creada";
                    break;
                case 'subscription.paid':
                    $record = "Subscripción pagada";
                    break;
                case 'subscription.canceled':
                    $record = "Subscripción cancelada";
                    break;
                case 'subscription.payment_failed':
                    $record = "Pago fallido de la subscripción";
                    break;
                case 'charge.created':
                    $record = "Cargo creado";
                    break;
                case 'charge.paid':
                    $record = "Cargo pagado";
                    break;
                case 'charge.refunded':
                    $record = "Cargo devuelto";
                    break;
                case 'charge.chargeback.created':
                    $record = "Devolucion del cargo creada";
                    break;
                case 'order.created':
                    $record = "Orden creada";
                    break;
                case 'order.paid':
                    $record = "Orden pagada";
                    break;
                case 'order.pending_payment':
                    $record = "Orden pendiente de pago";
                    break;
                case 'order.expired':
                    $record = "Orden expirada";
                    break;
                case 'plan.created':
                    $record = "Plan creado";
                    break;
                case 'plan.updated':
                    $record = "Plan actualizado";
                    break;
                case 'plan.deleted':
                    $record = "Plan eliminado";
                    break;
                default:
                    $record = $valor;
                    break;
            }
        }else if ($field == 'url_details') {
            $record = route('manager.conekta.log.dataTable', ['id' => $item['_id']]);
        }else if($field == 'data'){
            $jsonVal = $item['data'];
            $record =$jsonVal;
        }
        return $record;
    }

}
