<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Conekta\Manager\Models\ConektaOrder;

/**
 * Description of ConektaOrderDataTableController
 *
 * @author Luis
 */
class ConektaOrderDataTableController extends DataTableController{
    
    protected $collectionName = 'conektaOrder';
    protected $searchable = [
        'query' => [
            'orderID' => 'contains',
            'amount' => 'contains',
            'currency' => 'contains',
        ],
         'statusDescription' => 'contains',
    ];
    protected $fieldsDetails = [
        '_id' => 'Id',
        'orderID' => 'Identificador de la orden',
        'amount' => 'Monto de la orden',
        'currency' => 'Moneda',
        'statusDescription' => 'Estatus',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'Id',
        'orderID' => 'Identificador de la orden',
        'amount' => 'Monto de la orden',
        'currency' => 'Moneda',
        'statusDescription' => 'Estatus',
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        if ($field == 'amount') {
             $conversion = intval($item['amount'])/100;
            $record = '$' . number_format($conversion, 0, '.', ',');
        }else if($field == 'conekta_id'){
            $order = ConektaOrder::find((string) $item['_id']);
             if (isset($order->user->email)) {
                $record = "<a href=" . route('manager.user.index', ['s' => $order->user->email]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuario' > {$order->user->nombre} {$order->user->apellido} </a>";
            } else {
                $record = 'ninguno';
            }
        }else if ($field == 'url_details') {
            $record = route('manager.conekta.order.dataTable', ['id' => $item['_id']]);
        }else if ($field == 'statusDescription') {
            $value = $item['statusDescription'];
            switch ($value) {
                case 'pending_payment':
                    $record = "Pendiente de pago";
                    break;
                case 'declined':
                    $record = "Declinada";
                    break;
                case 'expired':
                    $record = "Expirada";
                    break;
                case 'paid':
                    $record = "Pagada";
                    break;
                case 'refunded':
                    $record = "Devolucion";
                    break;
                case 'partially_refunded':
                    $record = "Parcialmente devuelta";
                    break;
                case 'charged_back':
                    $record = "Cargo devuelto";
                    break;
                case 'pre_authorized':
                $record = "Preautorizada";
                    break;
                case 'voided':
                    $record = "Vacio";
                    break;

                default:
                    break;
            }
        }
        return $record;
    }
    
}
