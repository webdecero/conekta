<?php

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\ManagerController as WebdeceroManagerController;
use Conekta\Conekta as Conekta;

class ConektaBaseManager extends WebdeceroManagerController {

    protected $classConekta = \Webdecero\Conekta\Manager\Models\Conekta::class;
    protected $classConektaPlan = \Webdecero\Conekta\Manager\Models\ConektaPlan::class;
    protected $routeDetail = 'page.conekta.detail';
    protected $optionalParams = [];
    protected $configConekta;

    public function __construct() {

        parent::__construct();
        // setup Conekta api context
        $this->configConekta = config('manager.conekta.config');
        Conekta::setApiKey($this->configConekta['api_key']);
        Conekta::setApiVersion($this->configConekta['api_version']);
    }

    public function getMessageError($response) {

        $whitError = [];


        if (isset($response['conektaError']) && !empty($response['conektaError'])) {
            $whitError['error'] = current($response['conektaError']->details);
        } else {
            $whitError['error'] = $response['message'];
        }

        return $whitError;
    }

}
