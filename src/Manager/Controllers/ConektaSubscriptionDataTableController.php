<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Conekta\Manager\Models\ConektaSubscription;

/**
 * Description of ConektaSubscriptionDataTableController
 *
 * @author Luis
 */
class ConektaSubscriptionDataTableController extends DataTableController{
    
    protected $collectionName = 'conektaSubscription';
    protected $searchable = [
        'query' => [
            'subscriptionId' => 'contains',
            'status' => 'contains',
            'user_id' => 'contains'
        ],
        'statusDescription' => 'contains',
        'user_id' => 'contains'
    ];
    protected $fieldsDetails = [
        '_id' => 'Id',
        'subscriptionId' => 'Identificador de la subscripción',
        'transactionID' => 'Identificador de la transacción en conekta',
        'statusDescription' => 'Estatus',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'Id',
        'subscriptionId' => 'Identificador de la subscripción',
        'transactionID' => 'Identificador de la transacción en conekta',
        'statusDescription' => 'Estatus',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        $subscription = ConektaSubscription::find((string) $item['_id']);
        if ($field == 'conekta_id') {
           
            if(isset($subscription) || !empty($subscription)){
                if (isset($subscription->user->email)) {
                    $record = "<a href=" . route('manager.user.index', ['s' => $subscription->user->email]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuario' > {$subscription->user->nombre} {$subscription->user->apellido} </a>";
                } else {
                    $record = 'ninguno';
                }
            }else{
                $record = "WAITING ...";
            }
        }else if ($field == 'url_details') {
            $record = route('manager.conekta.subscription.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'plan_id') {
            
            if(isset($subscription) || !empty($subscription)){
                if (isset($subscription->conektaPlan->name)) {
                    $record = "<a href=" . route('manager.conekta.plan.index', ['s' => $subscription->conektaPlan->name]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuario' > {$subscription->conektaPlan->name} </a>";
                } else {
                    $record = 'ninguno';
                }
            }else{
                $record = "WAITING ...";
            }
            
        }else if ($field == 'pausar'){
            $value = $item['statusDescription'];
            if($value === 'active' || $value === 'in_trial'){
                $record = "<a href=" . route('manager.conekta.subscription.pause', ['id' => $item['conekta_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Pause' > <i class='fa fa-pause'></i> </a>";
            }
        }else if ($field == 'reanudar'){
            $value = $item['statusDescription'];
             if($value === 'paused'){
                $record = "<a href=" . route('manager.conekta.subscription.resume', ['id' => $item['conekta_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Reanudar' > <i class='fa fa-play'></i> </a>";
            }
        }else if ($field == 'cancelar'){
            $value = $item['statusDescription'];
             if($value === 'active'|| $value === 'paused' || $value === 'in_trial'){
                $record = "<a href=" . route('manager.conekta.subscription.cancel', ['id' => $item['conekta_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Cancelar' > <i class='fa fa-stop'></i> </a>";
            }
        }else if ($field == 'eventType'){
            $value = $item['eventType'];
            switch ($value) {
                case 'subscription.canceled':
                    $record = "Suscripcion Cancelada";
                    break;
                case 'subscription.created':
                    $record = "Suscripcion Creada";
                    break;

                default:
                  
                    break;
            }
        }else if($field == 'history'){
            
//             $userRelationships = config('manager.paypal.userRelationships');
//
//            $user = $userRelationships::find($item['_id']);
//            $numPays = $user->paypal()->count();
//
//            $record = "<a href=" . route('manager.paypal.index', ['s' => (string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Donativos' > {$numPays} <i class='fa fa-money'></i> </a>";
            
        }else if($field == 'statusDescription'){
            $value = $item['statusDescription'];
            switch ($value) {
                case 'in_trial':
                    $record = "En trial";
                    break;
                case 'active':
                    $record = "Activa";
                    break;
                case 'past_due':
                    $record = "Vencida";
                    break;
                case 'paused':
                    $record = "En pausa";
                    break;
                case 'canceled':
                    $record = "Cancelada";
                    break;

            }
        }
        else if($field == 'charges'){
            $userID = $subscription->user_id;
             
            $subscriptions = ConektaSubscription::where('user_id',$userID)->get();
            
            
            $numCharges = count($subscriptions);
            if($numCharges>0){
                
                $record = "<a href=" . route('manager.conekta.charge.index', ['s' => (string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Cargos' > {$numCharges} <i class='fa fa-credit-card'></i> </a>";
            }else{
                $record="WAITING...";
            }
        }
//        else if($field == 'pfail'){
//            
//        }
        return $record;
    }
}
