<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Controllers;

use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Conekta\Manager\Models\ConektaCharge;
use App\User;

/**
 * Description of ConektaChargeDataTableController
 *
 * @author Luis
 */
class ConektaChargeDataTableController extends DataTableController{
    protected $collectionName = 'conektaCharge';
    protected $searchable = [
        'query' => [
            'chargeId' => 'contains',
            'statusDescription' => 'contains',
            'description' => 'contains',
            'user_id' => 'contains',
            '_id' => 'contains'
        ],
        'statusDescription' => 'contains',
        'description' => 'contains',
        'user_id' => 'contains',
        '_id' => 'contains'
    ];
    protected $fieldsDetails = [
        '_id' => 'Id',
        'chargeId' => 'Identificador del cargo',
        'amount' => 'Monto del Cargo',
        'currency' => 'Moneda',
        'description' => 'Descripcion',
        'statusDescription' => 'Estatus',
        'type' => 'Tipo',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'Id',
        'chargeId' => 'Identificador del cargo',
        'amount' => 'Monto del Cargo',
        'currency' => 'Moneda',
        'description' => 'Descripcion',
        'statusDescription' => 'Estatus',
        'type' => 'Tipo',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación'
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        if ($field == 'conekta_id') {
            $charge = ConektaCharge::find((string) $item['_id']);
             if (isset($charge->user->email)) {
                $record = "<a href=" . route('manager.user.index', ['s' => $charge->user->email]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuario' > {$charge->user->nombre} {$charge->user->apellido} </a>";
            } else {
                $record = 'ninguno';
            }
        }else if ($field == 'url_details') {
            $record = route('manager.conekta.charge.dataTable', ['id' => $item['_id']]);
        }else if ($field == 'type'){
            $value = $item['description'];
            if($value === 'Payment from order'){
                $record = "Orden";
            }else{
                $record = "Subscripcion";
            }
            
        }else if ($field == 'statusDescription'){
            $value = $item['statusDescription'];
            switch ($value) {
                case 'pending_payment':
                    $record = "Pendiente";
                    break;
                case 'paid':
                    $record = "Pagado";
                    break;

                default:
                  
                    break;
            }
        }else if($field == 'subscription'){
            $charge = ConektaCharge::find($item['_id']);
            $subscription = $charge->conektaSubscription;
            if(isset($subscription) || !empty($subscription)){
                $record = "<a href=" . route('manager.conekta.subscription.index', ['s' => (string) $subscription->subscriptionId]) . "  class='text-center center-block' data-toggle='tooltip' title='Subscripcion' > {$subscription->subscriptionId} </a>";
            }else{
                $record = "WAITING ...";
            }
            
        }else if($field == 'amount'){
//            $conversion = intval($item['amount'])/100;
            $record = '$' . number_format($item['amount'], 0, '.', ',');
        }
        return $record;
    }
}
