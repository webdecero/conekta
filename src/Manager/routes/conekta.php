<?php

//conekta
Route::post('conekta/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.dataTable', 'uses' => 'ConektaDataTableController@dataTable'));
Route::resource('conekta', 'ConektaController', [
    'only' => [
        'index'
    ],
    'names' => [
        'index' => 'manager.conekta.index',
        'create' => 'manager.conekta.create',
        'store' => 'manager.conekta.store',
        'show' => 'manager.conekta.show',
        'edit' => 'manager.conekta.edit',
        'update' => 'manager.conekta.update',
        'destroy' => 'manager.conekta.destroy',
    ]
]);

//Plan Conekta
if (!Route::has('manager.conekta.plan.dataTable')) {
    Route::post('conekta/plan/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.plan.dataTable', 'uses' => 'ConektaPlanDataTableController@dataTable'));
}

if (!Route::has('manager.conekta.plan.destroy')) {
    Route::get('plan/{id}/delete', ['as' => 'manager.conekta.plan.destroy', 'uses' => 'ConektaPlanController@destroy']);
}

if (!Route::has('manager.conekta.plan.index')) {
    Route::get('plan/index', ['as' => 'manager.conekta.plan.index', 'uses' => 'ConektaPlanController@index']);
}

if (!Route::has('manager.conekta.plan.create')) {
    Route::get('plan/create', ['as' => 'manager.conekta.plan.create', 'uses' => 'ConektaPlanController@create']);
}
if (!Route::has('manager.conekta.plan.store')) {
    Route::post('plan/store', ['as' => 'manager.conekta.plan.store', 'uses' => 'ConektaPlanController@store']);
}
if (!Route::has('manager.conekta.plan.show')) {
    Route::get('plan/show', ['as' => 'manager.conekta.plan.show', 'uses' => 'ConektaPlanController@show']);
}
if (!Route::has('manager.conekta.plan.edit')) {
    Route::get('plan/edit/{id}', ['as' => 'manager.conekta.plan.edit', 'uses' => 'ConektaPlanController@edit']);
}
if (!Route::has('manager.conekta.plan.update')) {
    Route::put('plan/update/{id}', ['as' => 'manager.conekta.plan.update', 'uses' => 'ConektaPlanController@update']);
}




//Subscription Conekta


if (!Route::has('manager.conekta.subscription.dataTable')) {
    Route::post('conekta/subscription/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.subscription.dataTable', 'uses' => 'ConektaSubscriptionDataTableController@dataTable'));
}

if (!Route::has('manager.conekta.subscription.destroy')) {
    Route::get('subscription/{id}/delete', ['as' => 'manager.conekta.subscription.destroy', 'uses' => 'ConektaSubscriptionController@destroy']);
}

if (!Route::has('manager.conekta.subscription.index')) {
    Route::get('subscription/index', ['as' => 'manager.conekta.subscription.index', 'uses' => 'ConektaSubscriptionController@index']);
}

if (!Route::has('manager.conekta.subscription.create')) {
    Route::get('subscription/create', ['as' => 'manager.conekta.subscription.create', 'uses' => 'ConektaSubscriptionController@create']);
}
if (!Route::has('manager.conekta.subscription.store')) {
    Route::post('subscription/store', ['as' => 'manager.conekta.subscription.store', 'uses' => 'ConektaSubscriptionController@store']);
}
if (!Route::has('manager.conekta.subscription.show')) {
    Route::get('subscription/show', ['as' => 'manager.conekta.subscription.show', 'uses' => 'ConektaSubscriptionController@show']);
}
if (!Route::has('manager.conekta.subscription.edit')) {
    Route::get('subscription/edit', ['as' => 'manager.conekta.subscription.edit', 'uses' => 'ConektaSubscriptionController@edit']);
}
if (!Route::has('manager.conekta.subscription.update')) {
    Route::put('subscription/update', ['as' => 'manager.conekta.subscription.update', 'uses' => 'ConektaSubscriptionController@update']);
}

if (!Route::has('manager.conekta.subscription.pause')) {
    Route::put('subscription/pause/{id}', ['as' => 'manager.conekta.subscription.pause', 'uses' => 'ConektaSubscriptionController@pauseSubscriptionModule']);
}

if (!Route::has('manager.conekta.subscription.resume')) {
    Route::put('subscription/resume/{id}', ['as' => 'manager.conekta.subscription.resume', 'uses' => 'ConektaSubscriptionController@resumeSubscriptionModule']);
}

if (!Route::has('manager.conekta.subscription.cancel')) {
    Route::put('subscription/cancel/{id}', ['as' => 'manager.conekta.subscription.cancel', 'uses' => 'ConektaSubscriptionController@cancelSubscriptionModule']);
}

if (!Route::has('manager.conekta.subscription.create')) {
    Route::put('subscription/create/{id}', ['as' => 'manager.conekta.subscription.create', 'uses' => 'ConektaSubscriptionController@createSubscriptionModule']);
}

//Charges Conekta


if (!Route::has('manager.conekta.charge.dataTable')) {
    Route::post('conekta/charge/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.charge.dataTable', 'uses' => 'ConektaChargeDataTableController@dataTable'));
}

if (!Route::has('manager.conekta.charge.destroy')) {
    Route::get('charge/{id}/delete', ['as' => 'manager.conekta.charge.destroy', 'uses' => 'ConektaChargeController@destroy']);
}

if (!Route::has('manager.conekta.charge.index')) {
    Route::get('charge/index', ['as' => 'manager.conekta.charge.index', 'uses' => 'ConektaChargeController@index']);
}

if (!Route::has('manager.conekta.charge.create')) {
    Route::get('charge/create', ['as' => 'manager.conekta.charge.create', 'uses' => 'ConektaChargeController@create']);
}
if (!Route::has('manager.conekta.charge.store')) {
    Route::post('charge/store', ['as' => 'manager.conekta.charge.store', 'uses' => 'ConektaChargeController@store']);
}
if (!Route::has('manager.conekta.charge.show')) {
    Route::get('charge/show', ['as' => 'manager.conekta.charge.show', 'uses' => 'ConektaChargeController@show']);
}
if (!Route::has('manager.conekta.charge.edit')) {
    Route::get('charge/edit', ['as' => 'manager.conekta.charge.edit', 'uses' => 'ConektaChargeController@edit']);
}
if (!Route::has('manager.conekta.charge.update')) {
    Route::put('charge/update', ['as' => 'manager.conekta.charge.update', 'uses' => 'ConektaChargeController@update']);
}

//Order Conekta


if (!Route::has('manager.conekta.order.dataTable')) {
    Route::post('conekta/order/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.order.dataTable', 'uses' => 'ConektaOrderDataTableController@dataTable'));
}

if (!Route::has('manager.conekta.order.destroy')) {
    Route::get('order/{id}/delete', ['as' => 'manager.conekta.order.destroy', 'uses' => 'ConektaOrderController@destroy']);
}

if (!Route::has('manager.conekta.order.index')) {
    Route::get('order/index', ['as' => 'manager.conekta.order.index', 'uses' => 'ConektaOrderController@index']);
}

if (!Route::has('manager.conekta.order.create')) {
    Route::get('order/create', ['as' => 'manager.conekta.order.create', 'uses' => 'ConektaOrderController@create']);
}
if (!Route::has('manager.conekta.order.store')) {
    Route::post('order/store', ['as' => 'manager.conekta.order.store', 'uses' => 'ConektaOrderController@store']);
}
if (!Route::has('manager.conekta.order.show')) {
    Route::get('order/show', ['as' => 'manager.conekta.order.show', 'uses' => 'ConektaOrderController@show']);
}
if (!Route::has('manager.conekta.order.edit')) {
    Route::get('order/edit', ['as' => 'manager.conekta.order.edit', 'uses' => 'ConektaOrderController@edit']);
}
if (!Route::has('manager.conekta.order.update')) {
    Route::put('order/update', ['as' => 'manager.conekta.order.update', 'uses' => 'ConektaOrderController@update']);
}

// Logs Conekta

if (!Route::has('manager.conekta.log.dataTable')) {
    Route::post('conekta/log/dataTable/{id?}/{excel?}', array('as' => 'manager.conekta.log.dataTable', 'uses' => 'ConektaLogDataTableController@dataTable'));
}

if (!Route::has('manager.conekta.log.index')) {
    Route::get('log/index', ['as' => 'manager.conekta.log.index', 'uses' => 'ConektaLogController@index']);
}