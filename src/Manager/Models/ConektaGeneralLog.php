<?php

namespace Webdecero\Conekta\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class ConektaGeneralLog extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'conektaLog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        $configConektaUser = config('manager.conekta.userRelationships');
        return $this->belongsTo($configConektaUser);
    }
    
    public function conektaPlan(){
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaPlan::class);
    }

    public function conektaSubscriptions() {
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaSubscription::class);
    }
    
     public function conektaOrders() {
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaOrder::class);
    }
    
    public function conektaCharge(){
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaCharge::class);
    }
    
}
