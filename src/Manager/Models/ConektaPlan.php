<?php

namespace Webdecero\Conekta\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class ConektaPlan extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'conektaPlan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function conektaSubscriptions() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscription::class);
    }


}
