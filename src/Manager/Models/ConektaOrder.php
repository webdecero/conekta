<?php

namespace Webdecero\Conekta\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class ConektaOrder extends Base {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'conektaOrder';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function user() {
            $configConektaUser = config('manager.conekta.userRelationships');
            return $this->belongsTo($configConektaUser);
	}
}
