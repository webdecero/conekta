<?php

namespace Webdecero\Conekta\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class ConektaSubscription extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'conektaSubscription';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        $configConektaUser = config('manager.conekta.userRelationships');
        return $this->belongsTo($configConektaUser);
    }

    public function conektaPlan() {
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaPlan::class);
    }

    public function conektaLogs() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaGeneralLog::class);
    }
    
    public function conektaSubscriptionPayment() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscriptionPayment::class);
    }

     public function conektaSubscriptionPaymentFailed() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaSubscriptionPaymentFailed::class);
    }
    
      public function conektaCharge() {
        return $this->hasMany(\Webdecero\Conekta\Manager\Models\ConektaCharge::class);
    }
}
