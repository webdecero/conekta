<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Webdecero\Conekta\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

/**
 * Description of ConektaCustomer
 *
 * @author Luis
 */
class ConektaCharge extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'conektaCharge';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {

//         setup Conekta api context
        $configConektaUser = config('manager.conekta.userRelationships');

        return $this->belongsTo($configConektaUser);
    }

    public function conektaSubscription() {
        return $this->belongsTo(\Webdecero\Conekta\Manager\Models\ConektaSubscription::class);
    }

}
