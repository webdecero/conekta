<?php

namespace Webdecero\Conekta;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;

//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ConektaServiceProvider extends ServiceProvider {

    private $configConektaManager = __DIR__ . '/../config/manager/conekta/config.php';
    
    private $configConektaManagerPlan = __DIR__ . '/../config/manager/conekta/conektaPlan.php';
    private $folderViewsManager = __DIR__ . '/Manager/resources/views';
    private $folderLang = __DIR__ . '/Manager/resources/lang';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    
    public function boot() {


        $this->bootManager();
        $this->bootPages();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {




        $this->mergeConfigFrom($this->configConektaManager, 'manager.conekta.config');
        $this->mergeConfigFrom($this->configConektaManagerPlan, 'manager.conekta.conektaPlan');

//        $this->app->make('Webdecero\Paypal\Manager\Controllers\PaypalController');
    }

    private function bootManager() {

//        Publishes Configuración Conekta
//        php artisan vendor:publish --provider="Webdecero\Conekta\ConektaServiceProvider" --tag=config

        $this->publishes([
            $this->configConektaManager => config_path('manager/conekta/config.php'),
            $this->configConektaManagerPlan => config_path('manager/conekta/conektaPlan.php')
                ], 'config');
        
        
        
//        Registra Views
        $this->loadViewsFrom($this->folderViewsManager, 'conektaViews');

//ROUTE Conekta Manager
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Conekta\Manager\Controllers';

        $originalMiddleware = $config['middleware'];


        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/conekta.php');
        });
    }

    private function bootPages() {

//        php artisan vendor:publish --provider="Webdecero\Conekta\ConektaServiceProvider" --tag=lang 
        $this->loadTranslationsFrom($this->folderLang, 'ConektaLang');
        $this->publishes([
            $this->folderLang => resource_path('lang/vendor/ConektaLang'
            )], 'lang');

        
        
//        php artisan vendor:publish --provider="Webdecero\Conekta\ConektaServiceProvider" --tag=views 

//        $this->loadTranslationsFrom($this->folderLang, 'baseLang');
//        $this->publishes([
//            $this->folderLang => resource_path('lang/vendor/baseLang'
//            )], 'lang');
        
        
        //ROUTE Conekta Pages
       
        $config['namespace'] = 'Webdecero\Conekta\Pages\Controllers';


        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Pages/routes/conekta.php');
        });
        
        
        
        
    }

}
