<?php

return array(
//	Conekta SandBox
    'conekta_planDefault' => '',
//    'api_key' => 'key_ynWTrhYxxzP3Ksbhdz3Aeg', //Webdecero acount
    'api_key_front' => '',
    'api_key' => '',
    'conekta_planIdDefault' => '',
//	Conekta Live
//    'api_key_front' => '',
//    'api_key' => '',
    'api_version' => '2.0.0',
    'userRelationships' => App\User::class,
    'productRelationships' => App\User::class,
    'log.FileName' => storage_path() . '/logs/conekta.log'
);
