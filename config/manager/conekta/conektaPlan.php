<?php

$pages = [];

$pages['index'] = [
    'isSeo' => FALSE,
    'sectionName' => 'Administrador',
    'sections' => [
       
        'name' => [
            'element' => 'inputText',
            'label' => 'Nombre',
            'required' => true,
        ],
        'amount' => [
            'element' => 'inputText',
            'label' => 'Monto',
            'type' => 'number',
            'required' => true,
        ],
        'currency' => [
            'element' => 'inputSelect',
            'label' => 'Moneda',
            'required' => true,
            'options' => [
                'MXN' => 'Peso mexicano: MXN',
                'USD' => 'Dólar americano: USD'
            ],
        ],
        'interval' => [
            'element' => 'inputSelect',
            'label' => 'Cada',
            'required' => true,
            'options' => [
                '1' => '1',
                '2' => '2',
                '3' => '1',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12'
            ],
        ],
        'frequency' => [
            'element' => 'inputSelect',
            'label' => '',
            'required' => true,
            'options' => [
                'week' => 'Semana',
                'half_month' => 'Quincena',
                'month' => 'Mes',
                'year' => 'Año',
            ],
        ],
        'trialDays' => [
            'element' => 'inputText',
            'label' => 'Dias de Prueba',
            'type' => 'number',
            'required' => true,
        ],
    ]
];

$pages['editPlan'] = [
    'isSeo' => FALSE,
    'sectionName' => 'Administrador',
    'sections' => [
       
        'name' => [
            'element' => 'inputText',
            'label' => 'Nombre',
            'required' => true,
            'readonly' =>true
        ],
        'amount' => [
            'element' => 'inputText',
            'label' => 'Monto',
            'type' => 'number',
            'required' => true,
             'readonly' =>false
        ],
        'currency' => [
            'element' => 'inputSelect',
            'label' => 'Moneda',
            'required' => true,
            'options' => [
                'MXN' => 'Peso mexicano: MXN',
                'USD' => 'Dólar americano: USD'
            ],
             'readonly' =>true
        ],
        'interval' => [
            'element' => 'inputSelect',
            'label' => 'Cada',
            'required' => true,
            'options' => [
                '1' => '1',
                '2' => '2',
                '3' => '1',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12'
            ],
             'readonly' =>true
        ],
        'frequency' => [
            'element' => 'inputSelect',
            'label' => '',
            'required' => true,
            'options' => [
                'week' => 'Semana',
                'half_month' => 'Quincena',
                'month' => 'Mes',
                'year' => 'Año',
            ],
             'readonly' =>true
        ],
        'trialDays' => [
            'element' => 'inputText',
            'label' => 'Dias de Prueba',
            'type' => 'number',
            'required' => true,
            'readonly' =>true,
            'defaultValue' => 0
        ],
          
    ]
];

return $pages;

